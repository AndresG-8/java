
package infijosypostfijos;
/**
 * ESTE METODO LLAMA EL FORMULARIO,
 * EL CUAL HACE LO MISMO QUE LA CLASE InfijosYPostfijos.java
 * PERO QUE ES CON FRAMES
 * @author Gomez
 */
public class PpalForm {
    public static void main(String[] args){
        Infijos form = new Infijos();
        form.setVisible(true);
        form.setResizable(false);
    }
}

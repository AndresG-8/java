/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package infijosypostfijos;

import javax.swing.JOptionPane;

public class PilasVector {
    int tope;
    char postfijo[];//este contendra la expresion en postfijo y sera en la que apilaremos
    int n;
    
    int topeO;
    char infijo[];//este vec guarda la expresion en infijo
    char operadores[];//almacenara la pila con los operadores
    
    public PilasVector(int x) {//recibe el tamaño de la expresion en infijo
        n = x;//la guarda en n
  	postfijo = new char[n];//este vec esta vacio, solo se crea para mostrar la cantidad de datos
  	tope = - 1;//se hace a tope igual a 0 para iniciar el esa poscicion del vector...
        topeO = - 1;
        //inicializamos los vectores
        infijo = new char[n];//no se le puede decir que guarde en la posicion x ya que debe guardar en ottra diferente
        operadores = new char[n];
    }
    public void apilar(char d){//recibe una letra u operando del vecPostfijo en determinada posicion y procede
        if(tope == n){
            JOptionPane.showMessageDialog(null, "Pila llena");
        }else{
            tope = tope + 1;
            postfijo[tope] = d;
        }
    }
    public void apilarOperador(char d){//recibe una letra u operando del vecPostfijo en determinada posicion y procede
        topeO = topeO + 1;
        operadores[topeO] = d;
    }
    public void desapilar(char d){
        if(tope == -1){
            JOptionPane.showMessageDialog(null, "Pila vacia");
        }else{
            d = postfijo[tope];
            tope = tope - 1;
        }
    }
    public void desapilarOperador(){
        //3.Si es “)“ se desapila y se lleva el postfijo hasta encontrar el correspondiente “(”, 
        // el cual se desapila y no se lleva a postfijo
        //3.Si es “)“ no se apila y se busca el operador hasta encontrar el correspondiente “(”, 
        // el cual se desapila y no se lleva a postfijo
        for(int i = topeO; i >= 0; i--){//recorro el vector de atras hacia adelante para buscar el ( en caso de estarlo buscando
            if(operadores[i] == '('){
                topeO = topeO - 1;
            }else{
                tope = tope + 1;
                postfijo[tope] = operadores[i];
                topeO = topeO - 1;
            }
        }
    }
       
    public void mostrar(){//metodo llamado desde InfijoYPostfijo
        String m = "";
        for(int i = 0; i < n; i++){
            m += postfijo[i] + "   ";
        }
        JOptionPane.showMessageDialog(null, m.toUpperCase()); 
    }
     public String mostrarTwo(){//metodo llamado desde InfijoYPostfijo
        String m = "";
        for(int i = 0; i < n; i++){
            m += postfijo[i] + "   ";
        }
        return m.toUpperCase();
    }
    public void caracter(char y){
        switch(y){//Si es un operador
            case '(':
                if(topeO == - 1){
                    apilarOperador(y);
                }else{
                    while(priDentro() >= priFuera(y)){
                        desapilarOperador();
                    }
                    apilarOperador(y);
                }
                break;
            case '+':
                if(topeO == - 1){
                    apilarOperador(y);
                }else{
                    while(priDentro() >= priFuera(y)){
                        desapilarOperador();
                    }
                    apilarOperador(y);
                }
                break;
            case '-':
                 if(topeO == - 1){
                    apilarOperador(y);
                }else{
                    while(priDentro() >= priFuera(y)){
                        desapilarOperador();
                    }
                    apilarOperador(y);
                }
                break;    
            case '*':
                if(topeO == - 1){
                    apilarOperador(y);
                }else{
                    while(priDentro() >= priFuera(y)){
                       desapilarOperador();
                    }
                    apilarOperador(y);
                }
                break;
            case '/':
                if(topeO == - 1){
                    apilarOperador(y);
                }else{
                    while(priDentro() >= priFuera(y)){
                        desapilarOperador();
                    }
                    apilarOperador(y);
                }
                break;    
            case '^':
                if(topeO == - 1){
                    apilarOperador(y);
                }else{
                    while(priDentro() >= priFuera(y)){
                        desapilarOperador();
                    }
                    apilarOperador(y);
                }
                break;
            case ')':
                desapilarOperador();
                break;
            default://si es un operando, se pasa directamente a postfijo
                apilar(y);//los apila en el vector postfijo
        }
    }
   
   public int priDentro(){
        int prioridadIn;
        if(topeO == - 1){
            return 0;
        }else{
            switch(operadores[topeO]){
                case '(':
                    prioridadIn = 0;
                    break;
                case '+':
                    prioridadIn = 1;
                    break;
                case '-':
                    prioridadIn = 1;
                    break;    
                case '*':
                    prioridadIn = 2;
                    break;
                case '/':
                    prioridadIn = 2;
                    break;    
                case '^':
                    prioridadIn = 3;
                    break;

                default:
                    prioridadIn = - 1;
            }
        }
        
       return prioridadIn;
   }
   public int priFuera(char y){
        int prioridadOut;
        switch(y){
            case '(':
                prioridadOut = 4;
                break;
            case '+':
                prioridadOut = 1;
                break;
            case '-':
                prioridadOut = 1;
                break;    
            case '*':
                prioridadOut = 2;
                break;
            case '/':
                prioridadOut = 2;
                break;    
            case '^':
                prioridadOut = 3;
                break;

            default:
                prioridadOut = - 1;
        }
       return prioridadOut;
   }
}
/*1.	Si es un operando se pasa directamente a posfijo... SE APILA en vec
2.	Si es un operador...
    a.	Mientras la prioridad dentro de la pila del tope sea >= a la prioridad  
           fuera de la pila del operador se desapila y se lleva a postfijo
    b.          Se apila el operador
3.	Si es “)“ se desapila y se lleva el postfijo hasta encontrar el correspondiente 
        “(”, el cual se desapila y no se lleva a postfijo
4.	Al terminar se desapila todo y se lleva a postfijo

        */
  
 
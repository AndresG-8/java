package infijosypostfijos;

import javax.swing.JOptionPane;

/**
 *
 * @author Gomez
 */
public class InfijosYPostfijos {
    public static void main(String[] args) {
        String cadena;
        cadena = JOptionPane.showInputDialog("Ingrese la expresión en Infijo");
        int x = cadena.length();
        PilasVector form = new PilasVector(x);
        for(int i = 0; i < x; i++){
            char d = cadena.charAt(i);
            form.caracter(d);
        }
        form.desapilarOperador();
        form.mostrar();
        
        
    }
}
